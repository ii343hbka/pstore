import json

from fastapi import APIRouter, Depends, HTTPException, Body, status
from sqlalchemy.orm import Session
from fastapi.responses import JSONResponse

from app.db import get_db
from app.db.models import Storage
from app.schemas import ResponseBoolSchema
from app.storage_crud import StorageCrud

storage_router = APIRouter(
    prefix='/bucket',
    tags=['bucket'],
)


@storage_router.post(
    '/',
    summary='Insert data',
)
def insert_data(
    payload: dict = Body(...),
    db: Session = Depends(get_db),
):
    data_dict = Storage(
        data=json.dumps(payload)
    )

    return json.loads(StorageCrud(db=db).insert_data(data=data_dict).data)


@storage_router.get(
    '/{data_id}',
    summary='Get data',
)
def get_data(
    data_id: int,
    db: Session = Depends(get_db),
):
    data = StorageCrud(db=db).get_data_by_id(data_id=data_id)

    if data is None:
        raise HTTPException(status_code=400, detail="Data with provided id does not exist")

    return json.loads(data.data)


@storage_router.patch(
    '/{data_id}',
    summary='Update data',
)
def update_data(
    data_id: int,
    payload: dict = Body(...),
    db: Session = Depends(get_db),
):
    storage_db = StorageCrud(db=db)
    data = storage_db.get_data_by_id(data_id=data_id)

    if data is None:
        raise HTTPException(status_code=400, detail="Data with provided id does not exist")

    return json.loads(storage_db.update_data(data=data, data_dict=payload).data)


@storage_router.delete(
    '/{data_id}',
    summary='Delete data',
    response_model=ResponseBoolSchema,
)
def delete_data(
    data_id: int,
    db: Session = Depends(get_db),
):
    storage_db = StorageCrud(db=db)
    data = storage_db.get_data_by_id(data_id=data_id)

    if data is None:
        raise HTTPException(status_code=400, detail="Data with provided id does not exist")

    storage_db.delete_data(data=data)

    return JSONResponse(
        {'result': True},
        status_code=status.HTTP_200_OK,
    )
