import sqlalchemy as sa

from app.db.base import Base


class Storage(Base):
    __tablename__ = 'Storage'

    id = sa.Column(sa.Integer, primary_key=True)
    data = sa.Column(sa.JSON, nullable=True)
