import typing as tp
from pydantic import BaseModel, Field


# class DataSchema(BaseModel):
#     data: tp.Optional[dict] = Field(None, title='Data')
#
#     class Config:
#         orm_mode = True


class ResponseBoolSchema(BaseModel):
    result: bool = Field(True)
