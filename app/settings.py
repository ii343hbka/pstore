import os
from pathlib import Path

from pydantic import BaseSettings


class Settings(BaseSettings):
    """Class with validating project settings"""

    # Project info
    PROJECT_NAME: str = 'Storage'

    # DB
    DB_USER = os.getenv('DB_USER', 'storage')
    DB_PASSWORD = os.getenv('DB_PASSWORD', 'storage')
    DB_HOST = os.getenv('DB_HOST', 'localhost')
    DB_PORT = os.getenv('DB_PORT', 5432)
    DB_NAME = os.getenv('DB_NAME', 'storage_db')
    DB_URL = f'postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}'

    DEBUG: bool = os.getenv('DEBUG', True)

    BASE_DIR = Path(__file__).resolve().parent.parent

    class Config:
        case_sensitive = True


settings = Settings()
