import json
import typing as tp

from sqlalchemy import select

from app.db.models import Storage


class StorageCrud:
    def __init__(self, db):
        self.db = db

    def get_data_by_id(self, data_id: int) -> tp.Optional[Storage]:
        query = self.db.execute(select(Storage).filter(Storage.id == data_id))
        self.db.commit()
        data = query.scalars().first()
        return data

    def insert_data(self, data: Storage):
        self.db.add(data)
        self.db.commit()
        self.db.refresh(data)
        return data

    def update_data(self, data: Storage, data_dict: tp.Optional[dict] = None):
        data.data = json.dumps(data_dict)
        self.db.commit()
        return data

    def delete_data(self, data: Storage) -> None:
        self.db.delete(data)
        self.db.commit()
