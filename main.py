from fastapi import FastAPI

from app.api.storage import storage_router
from app.settings import settings


app = FastAPI(
    title=settings.PROJECT_NAME,
    version='0.0.1',
    debug=settings.DEBUG,
)

app.include_router(storage_router)

# local run with ide debugger
if __name__ == '__main__':
    from uvicorn import run

    run(app=app, host='0.0.0.0', port=8001, log_level='info')
